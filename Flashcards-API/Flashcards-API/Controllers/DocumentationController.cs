﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Flashcards.API.Controllers
{
    [Route("api/[controller]")]
    public class DocumentationController : Controller
    {
        public IActionResult GetDocumentation()
        {
            return View();
        }

    }
}
