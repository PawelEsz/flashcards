﻿using Flashcards.API.Domain.Models;
using Flashcards.API.Domain.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Flashcards.API.Controllers
{
    [Route("api/[controller]")]
    public class FlashcardController : Controller
    {
        private readonly FlashcardService _flashcardService;

        public FlashcardController()
        {
            _flashcardService = new FlashcardService();
        }

        [HttpPost]
        public IActionResult AddFlashcard([FromBody] Flashcard flashcard)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            var result = _flashcardService.AddFlashcard(flashcard);

            if (result == null)
            {
                return BadRequest();
            }

            return Ok(result);
        }

        [HttpPut]
        public IActionResult ChangeIsLearned([FromBody] Flashcard flashcard)
        {
            if(!ModelState.IsValid)
            {
                return BadRequest();
            }

            var result = _flashcardService.ChangeIsLearned(flashcard);

            if(!result)
            {
                return BadRequest();
            }

            return NoContent();
        }

    }
}
