﻿using Flashcards.API.Domain.Model;
using Flashcards.API.Domain.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Flashcards.API.Controllers
{
    [Route("api/Login")]
    public class LoginController : Controller
    {
        private readonly UserService _userService;

        public LoginController()
        {
            _userService = new UserService();
        }

        [HttpPost]
        public IActionResult Login([FromBody] User user)
        {
            if (user.Login == null || user.Password == null)
                return BadRequest();
            
            User user_result = _userService.Login(user);

            if (user_result != null)
                return Ok(user_result);

            return BadRequest();
        }
    }
}
