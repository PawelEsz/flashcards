﻿using Flashcards.API.Domain.Model;
using Flashcards.API.Domain.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Flashcards.API.Controllers
{
    [Route("api/[controller]")]
    public class SynchronizationController : Controller
    {
        private readonly UserService _userService;
        private readonly SynchronizationService _synchronizationService;

        public SynchronizationController()
        {
            _userService = new UserService();
            _synchronizationService = new SynchronizationService();
        }

        //[HttpGet] //# do zrobienia przez Wojtka :); 
        //public IActionResult Synchronize([FromBody] User user)
        //{
        //    return Ok(_synchronizationService.Synchronize(user));
        //}

    }
}
