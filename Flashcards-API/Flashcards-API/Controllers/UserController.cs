﻿using Flashcards.API.Domain.Model;
using Flashcards.API.Domain.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Flashcards.API.Controllers
{
    [Route("api/[controller]")]
    public class UserController : Controller
    {
        private readonly UserService _userService;

        public UserController()
        {
            _userService = new UserService();
        }

        [HttpGet]
        public IActionResult GetUser()
        {
            var user =_userService.GetUser();

            if(user != null)
            {
                return Ok(user);
            }

            return Ok(1);
        }
    }
}
