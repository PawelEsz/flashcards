﻿using Flashcards.API.Domain.Model;
using Flashcards.API.Domain.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Flashcards.API.Controllers
{
    [Route("api/[controller]")]
    public class RegisterController : Controller
    {
        private readonly UserService _userService;

        public RegisterController()
        {
            _userService = new UserService();
        }

        [HttpPost]
        public IActionResult Register([FromBody] User user)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            if (_userService.Register(user))
            {
                return NoContent();
            }

            return BadRequest();
        }
    }
}
