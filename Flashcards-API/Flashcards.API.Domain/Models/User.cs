﻿using Flashcards.API.Domain.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Flashcards.API.Domain.Model
{
    public class User
    {
        [Key]
        public int Id { get; set; }

        public string Token { get; set; }

        [Required]
        public string Email { get; set; }

        [Required]
        public string Login { get; set; }

        [Required]
        public string Password { get; set; }

        public string Photo { get; set; }

        [Required]
        public bool IsLearn { get; set; }

        public string Region { get; set; }

        public string City { get; set; }

        public virtual ICollection<List> Lists { get; set; }

        //public virtual ICollection<PrivateList> PrivateLists { get; set; }
    }
}
