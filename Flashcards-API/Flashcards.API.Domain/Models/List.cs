﻿using Flashcards.API.Domain.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Flashcards.API.Domain.Models
{
    public class List
    {
        [Key]
        public int Id { get; set; }

        public int UserId { get; set; }

        public virtual User User { get; set; }

        [Required]
        public string Name { get; set; }

        public bool IsPublic { get; set; }

        //public virtual PrivateList PrivateList { get; set; }

        public ICollection<Flashcard> Flashcards { get; set; }
    }
}
