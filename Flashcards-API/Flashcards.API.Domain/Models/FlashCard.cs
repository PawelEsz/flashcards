﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Flashcards.API.Domain.Models
{
    public class Flashcard
    {
        [Key]
        public int Id { get; set; }

        public int ListId { get; set; }

        public string PolishFlashcard { get; set; }

        public string EnglishFlashcard { get; set; }

        public bool IsLearned { get; set; }

        public virtual List List { get; set; }
    }
}
