﻿using Flashcards.API.Domain.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Flashcards.API.Domain.Models
{
    public class PrivateList
    {
        [Key]
        public int Id { get; set; }

        public int ListId { get; set; }

        public int UserId { get; set; }

        public string Taught { get; set; }

        public virtual User User { get; set; }

        public virtual List List { get; set; }

        public virtual ICollection<List> Lists { get; set; }
    }
}
