﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Flashcards.API.Domain.Models
{
    public class LoginState
    {
        [Key]
        public int Id { get; set; }

        public int UserId { get; set; }

        public string Token { get; set; }

        public int SessionTime { get; set; }

    }
}
