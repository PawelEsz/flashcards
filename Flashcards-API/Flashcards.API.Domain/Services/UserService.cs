﻿using Flashcards.API.Domain.Model;
using Flashcards.API.Domain.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Flashcards.API.Domain.Services
{
    public class UserService
    {
        private readonly FlashcardsContext _context;
        private readonly AuthenticationService _authenticationService;

        public UserService()
        {
            _context = new FlashcardsContext();
            _authenticationService = new AuthenticationService();
        }

        public bool Register(User user)
        {
            if(!_context.Users.Any(u => u.Email == user.Email || u.Login == user.Login))
            {
                _context.Users.Add(user);
                var result = _context.SaveChanges();
                return result > 0;
            }

            return false;
        }

        public User Login(User user)
        {
            User result = _context.Users
                .Include(l => l.Lists).ThenInclude(f => f.Flashcards).
                SingleOrDefault(u => u.Login == user.Login && u.Password == user.Password);

            if(result == null)
            {
                return null;
            }

            var resultUser = new User()
            {
                Id = result.Id,
                Email = result.Email,
                Login = result.Login,
                City = result.City,
                IsLearn = result.IsLearn,
                Password = result.Password,
                Region = result.Region,
                Photo = result.Photo,
                //Token = _authenticationService.GenerateToken(),
                Lists = result.Lists.Select(l => new List()
                {
                    Id = l.Id,
                    UserId = l.UserId,
                    IsPublic = l.IsPublic,
                    Name = l.Name,
                    Flashcards = l.Flashcards.Select(f => new Flashcard()
                    {
                        Id = f.Id,
                        EnglishFlashcard = f.EnglishFlashcard,
                        PolishFlashcard = f.PolishFlashcard,
                        ListId = f.ListId,
                        IsLearned = f.IsLearned
                    }).ToList()
                }).ToList()
            };

            //resultUser.Token = _authenticationService.GenerateToken();
            //_authenticationService.CreateLoginState(resultUser.Id, resultUser.Token);

            return resultUser;       
        }

        public User GetUser()
        {
            var user = _context.Users.FirstOrDefault();

            if(user != null)
            {
                return user;
            }

            return null;
        }


    }
}
