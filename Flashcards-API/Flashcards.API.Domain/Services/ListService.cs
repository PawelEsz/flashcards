﻿using Flashcards.API.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Flashcards.API.Domain.Services
{
    public class ListService
    {
        private readonly FlashcardsContext _context;

        public ListService()
        {
            _context = new FlashcardsContext();
        }

        public List AddList(List list)
        {
            if (!_context.Users.Any(x => x.Id == list.UserId))
            {
                return null;
            }

            _context.Lists.Add(list);
            _context.SaveChanges();

            var result = new List
            {
                Id = list.Id,
                Name = list.Name,
                IsPublic = list.IsPublic,
                UserId = list.UserId,
            };

            return result;
        }
    }
}
