﻿using Flashcards.API.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Flashcards.API.Domain.Services
{
    public class AuthenticationService
    {
        private readonly FlashcardsContext _context;

        public AuthenticationService()
        {
            _context = new FlashcardsContext();
        }

        public void CreateAuthentication()
        {

        }

        public string GenerateToken()
        {
            string token;
            do
            {
                token = new Guid().ToString();
                if (_context.LoginStates.Any(u => u.Token == token))
                    token = null;
            } while (token != null);

            return token;
        }

        public void CreateLoginState(int id, string token)
        {
            LoginState loginState = new LoginState
            {
                UserId = id,
                Token = token
            };

            _context.LoginStates.Add(loginState);

            _context.SaveChanges();
        }


    }
}
