﻿using Flashcards.API.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Flashcards.API.Domain.Services
{
    public class FlashcardService
    {
        private readonly FlashcardsContext _context;

        public FlashcardService()
        {
            _context = new FlashcardsContext();
        }

        public Flashcard AddFlashcard(Flashcard flashcard)
        {
            if (!_context.Lists.Any(x => x.Id == flashcard.ListId))
            {
                return null;
            }

            _context.Flashcards.Add(flashcard);
            _context.SaveChanges();

            var result = new Flashcard
            {
                Id = flashcard.Id,
                EnglishFlashcard = flashcard.EnglishFlashcard,
                PolishFlashcard = flashcard.PolishFlashcard,
                ListId = flashcard.ListId
            };

            return result;
        }

        public bool ChangeIsLearned(Flashcard flashcard)
        {
            if (!_context.Flashcards.Any(x => x.Id == flashcard.Id))
            {
                return false;
            }

            var result = _context.Flashcards.SingleOrDefault(f => f.Id == flashcard.Id);

            if(result == null || result.IsLearned == flashcard.IsLearned)
            {
                return false;
            }

            result.IsLearned = flashcard.IsLearned;

            return _context.SaveChanges() > 0;
        }

        //public bool AddFlashcard(Flashcard flashcard)
        //{
        //    if (!_context.Lists. .Any(u => u.Email == user.Email || u.Login == user.Login))
        //    {
        //        _context.Users.Add(user);
        //        var result = _context.SaveChanges();
        //        return result > 0;
        //    }

        //    return false;
        //}
    }
}
