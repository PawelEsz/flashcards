﻿using Flashcards.API.Domain.Model;
using Flashcards.API.Domain.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Flashcards.API.Domain
{
    public class FlashcardsContext : DbContext
    {
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            //optionsBuilder.UseSqlServer(@"Server=(localdb)\mssqllocaldb;Database=Flashcards;Trusted_Connection=True;");
            optionsBuilder.UseSqlServer(@"Server=tcp:flashcards.database.windows.net,1433;Initial Catalog=Flashcards;Persist Security Info=False;User ID=Grupa8;Password=InzynierySieKsztalco!;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;");
        }

        public DbSet<User> Users { get; set; }

        public DbSet<List> Lists { get; set; }

        public DbSet<Flashcard> Flashcards { get; set; }

        public DbSet<LoginState> LoginStates { get; set; }

        //public DbSet<PrivateList> PrivateLists { get; set; }
    }
}
