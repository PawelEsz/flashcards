﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Flashcards.API.Domain.Migrations
{
    public partial class removeprivateList : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Lists_PrivateLists_PrivateListId",
                table: "Lists");

            migrationBuilder.DropTable(
                name: "PrivateLists");

            migrationBuilder.DropIndex(
                name: "IX_Lists_PrivateListId",
                table: "Lists");

            migrationBuilder.DropColumn(
                name: "PrivateListId",
                table: "Lists");

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "Lists",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddColumn<int>(
                name: "UserId",
                table: "Lists",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Lists_UserId",
                table: "Lists",
                column: "UserId");

            migrationBuilder.AddForeignKey(
                name: "FK_Lists_Users_UserId",
                table: "Lists",
                column: "UserId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Lists_Users_UserId",
                table: "Lists");

            migrationBuilder.DropIndex(
                name: "IX_Lists_UserId",
                table: "Lists");

            migrationBuilder.DropColumn(
                name: "UserId",
                table: "Lists");

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "Lists",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AddColumn<int>(
                name: "PrivateListId",
                table: "Lists",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "PrivateLists",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ListId = table.Column<int>(nullable: false),
                    Taught = table.Column<string>(nullable: true),
                    UserId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PrivateLists", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PrivateLists_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Lists_PrivateListId",
                table: "Lists",
                column: "PrivateListId");

            migrationBuilder.CreateIndex(
                name: "IX_PrivateLists_UserId",
                table: "PrivateLists",
                column: "UserId");

            migrationBuilder.AddForeignKey(
                name: "FK_Lists_PrivateLists_PrivateListId",
                table: "Lists",
                column: "PrivateListId",
                principalTable: "PrivateLists",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
